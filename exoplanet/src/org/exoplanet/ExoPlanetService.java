package org.exoplanet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.TreeMap;

import org.exoplanet.enums.ExoPlanet;
import org.exoplanet.enums.ExoPlanetDashboard;
import org.exoplanet.enums.ExoPlanetDashboard.PlanetSizeCount;
import org.exoplanet.enums.TypeFlag;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ExoPlanetService {

	public static void main(String[] args) {

		try {
			InputStream stream = ClassLoader.getSystemResourceAsStream("exoplanet.json");;
			if(args.length==1){
				stream = new FileInputStream(new File(args[0]));
			}
			
			String result = exoPlantDashboard(stream);

			System.out.println("Result");
			System.err.println(result);
		} catch (ArrayIndexOutOfBoundsException|IllegalArgumentException|IOException e) {
			System.out.println("Error");
			e.printStackTrace();
		}
	}

	public static String exoPlantDashboard(InputStream stream) throws JsonParseException, JsonMappingException, IOException {
		String resultJson = null;
	
		// Initialize object mapper.
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		// Deserialize JSON string to java object.
		List<ExoPlanet> exoPlanets = mapper.readValue(stream, new TypeReference<List<ExoPlanet>>() {});

		// Create dashboard reference.
		ExoPlanetDashboard dashboard = new ExoPlanetDashboard();
		dashboard.setPlanetTimelineBySize(new TreeMap<Integer, PlanetSizeCount>());

		int hotStarMaxTemp = 0;
		for (ExoPlanet exoPlanet : exoPlanets) {
			// Add/Fetch PlanetSizeCount reference.
			PlanetSizeCount planetSizeCount = dashboard.getPlanetTimelineBySize()
					.computeIfAbsent(exoPlanet.getDiscoveryYear(), year -> {
						PlanetSizeCount _planetSizeCount = new PlanetSizeCount();
						dashboard.getPlanetTimelineBySize().put(year, _planetSizeCount);
						return _planetSizeCount;
					});

			// Check for ORPHAN planets
			if (TypeFlag.ORPHAN.equals(exoPlanet.getTypeFlag())) {
				dashboard.setOrphanPlanetCount(dashboard.getOrphanPlanetCount() + 1);
			}
			
			// Check for planet with hotest star.
			if (hotStarMaxTemp < exoPlanet.getHostStarTempK()) {
				hotStarMaxTemp = exoPlanet.getHostStarTempK();
				dashboard.setHostarPlanetIdentifier(exoPlanet.getPlanetIdentifier());
			}
			
			// Check planet size.
			if (exoPlanet.getRadiusJpt() < 1) {
				planetSizeCount.setSmallPlanetCount(planetSizeCount.getSmallPlanetCount() + 1);

			} else if (exoPlanet.getRadiusJpt() < 2) {
				planetSizeCount.setMediumPlanetCount(planetSizeCount.getMediumPlanetCount() + 1);

			} else {
				planetSizeCount.setLargePlanetCount(planetSizeCount.getLargePlanetCount() + 1);

			}
		}
		resultJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(dashboard);
		return resultJson;

	}
}
