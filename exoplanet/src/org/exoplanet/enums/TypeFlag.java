package org.exoplanet.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TypeFlag {
	UNKNOWN(0), PTYPE(1), STYPE(2), ORPHAN(3);

	private int typeCode;

	TypeFlag(int typeCode) {
		this.typeCode = typeCode;
	}

	public int getTypeCode() {
		return typeCode;
	}

	@JsonCreator
	public static TypeFlag forValue(int typeCode) {
		for (TypeFlag typeFlag : TypeFlag.values()) {
			if (typeCode == typeFlag.getTypeCode()) {
				return typeFlag;
			}
		}
		return UNKNOWN;
	}

	@JsonValue
	public int toValue() {

		return this.getTypeCode();
	}
}
