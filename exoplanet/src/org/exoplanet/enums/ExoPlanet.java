package org.exoplanet.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
public class ExoPlanet {
	@JsonProperty("PlanetIdentifier")
	private String planetIdentifier;
	
	@JsonProperty("TypeFlag")
	private TypeFlag typeFlag;
	
	@JsonProperty("HostStarTempK")
	private int hostStarTempK;

	@JsonProperty("DiscoveryYear")
	private int discoveryYear;
	
	@JsonProperty("RadiusJpt")
	private double radiusJpt;

	public String getPlanetIdentifier() {
		return planetIdentifier;
	}

	public void setPlanetIdentifier(String planetIdentifier) {
		this.planetIdentifier = planetIdentifier;
	}

	public TypeFlag getTypeFlag() {
		return typeFlag;
	}

	public void setTypeFlag(TypeFlag typeFlag) {
		this.typeFlag = typeFlag;
	}

	public int getHostStarTempK() {
		return hostStarTempK;
	}

	public void setHostStarTempK(int hostStarTempK) {
		this.hostStarTempK = hostStarTempK;
	}

	public int getDiscoveryYear() {
		return discoveryYear;
	}

	public void setDiscoveryYear(int discoveryYear) {
		this.discoveryYear = discoveryYear;
	}

	public double getRadiusJpt() {
		return radiusJpt;
	}

	public void setRadiusJpt(double radiusJpt) {
		this.radiusJpt = radiusJpt;
	}
	
}
