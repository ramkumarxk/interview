package org.exoplanet.enums;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExoPlanetDashboard {

	@JsonProperty("OrphanPlanetCount")
	private int orphanPlanetCount;

	@JsonProperty("HostarPlanetIdentifier")
	private String hostarPlanetIdentifier;

	@JsonProperty("PlantTimelineBySize")
	private Map<Integer, PlanetSizeCount> planetTimelineBySize;

	public int getOrphanPlanetCount() {
		return orphanPlanetCount;
	}

	public void setOrphanPlanetCount(int orphanPlanetCount) {
		this.orphanPlanetCount = orphanPlanetCount;
	}

	public String getHostarPlanetIdentifier() {
		return hostarPlanetIdentifier;
	}

	public void setHostarPlanetIdentifier(String hostarPlanetIdentifier) {
		this.hostarPlanetIdentifier = hostarPlanetIdentifier;
	}

	public Map<Integer, PlanetSizeCount> getPlanetTimelineBySize() {
		return planetTimelineBySize;
	}

	public void setPlanetTimelineBySize(Map<Integer, PlanetSizeCount> planetTimelineBySize) {
		this.planetTimelineBySize = planetTimelineBySize;
	}

	public static class PlanetSizeCount {
		private int smallPlanetCount;
		private int mediumPlanetCount;
		private int largePlanetCount;

		public int getSmallPlanetCount() {
			return smallPlanetCount;
		}

		public void setSmallPlanetCount(int smallPlanetCount) {
			this.smallPlanetCount = smallPlanetCount;
		}

		public int getMediumPlanetCount() {
			return mediumPlanetCount;
		}

		public void setMediumPlanetCount(int mediumPlanetCount) {
			this.mediumPlanetCount = mediumPlanetCount;
		}

		public int getLargePlanetCount() {
			return largePlanetCount;
		}

		public void setLargePlanetCount(int largePlanetCount) {
			this.largePlanetCount = largePlanetCount;
		}

	}
}
